
# /Volumes/Untitled/tools/qr-cli-gen/mont.sh

convert $1 -resize 407x407^ ,a1.png

cp ,a1.png ,t_q0000.png
cp ,a1.png ,t_q0001.png
cp ,a1.png ,t_q0002.png
cp ,a1.png ,t_q0003.png
cp ,a1.png ,t_q0004.png
cp ,a1.png ,t_q0005.png
cp ,a1.png ,t_q0006.png

cp ,a1.png ,t_q0010.png
cp ,a1.png ,t_q0011.png
cp ,a1.png ,t_q0012.png
cp ,a1.png ,t_q0013.png
cp ,a1.png ,t_q0014.png
cp ,a1.png ,t_q0015.png
cp ,a1.png ,t_q0016.png

cp ,a1.png ,t_q0010.png
cp ,a1.png ,t_q0011.png
cp ,a1.png ,t_q0012.png
cp ,a1.png ,t_q0013.png
cp ,a1.png ,t_q0014.png
cp ,a1.png ,t_q0015.png
cp ,a1.png ,t_q0016.png

cp ,a1.png ,t_q0020.png
cp ,a1.png ,t_q0021.png
cp ,a1.png ,t_q0022.png
cp ,a1.png ,t_q0023.png
cp ,a1.png ,t_q0024.png
cp ,a1.png ,t_q0025.png
cp ,a1.png ,t_q0026.png

cp ,a1.png ,t_q0030.png
cp ,a1.png ,t_q0031.png
cp ,a1.png ,t_q0032.png
cp ,a1.png ,t_q0033.png
cp ,a1.png ,t_q0034.png
cp ,a1.png ,t_q0035.png
cp ,a1.png ,t_q0036.png

cp ,a1.png ,t_q0040.png
cp ,a1.png ,t_q0041.png
cp ,a1.png ,t_q0042.png
cp ,a1.png ,t_q0043.png
cp ,a1.png ,t_q0044.png
cp ,a1.png ,t_q0045.png
cp ,a1.png ,t_q0046.png


montage ,t_q????.png \
    -geometry 407x407\>+21+21 \
	out.png

# works - with 333x333 size=9 QR Code
# -geometry 333x333\>+58+58 

convert -rotate 90 out.png out-portrate.png

## convert out.png -crop 3150x2250+75+75\!  out6.png
