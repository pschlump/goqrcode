module gitlab.com/pschlump/goqrcode

go 1.12

require (
	github.com/pschlump/MiscLib v1.0.0 // indirect
	github.com/pschlump/godebug v1.0.1
	github.com/pschlump/json v1.12.0 // indirect
)
